package br.com.brasilprev.mock.pisys5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@EnableJpaRepositories
@SpringBootApplication
@ComponentScan(basePackages = { "br.com.brasilprev.mock"})
public class PocConcedidosMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocConcedidosMsApplication.class, args);
	}

}
