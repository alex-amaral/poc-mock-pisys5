package br.com.brasilprev.mock.pisys5;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Getter
@AllArgsConstructor
public class Preconceder extends AbstractPersistable<Long> {

    private String matricula;
    private String beneficiario;
    private BigDecimal valorBeneficio;
    private String tipoRenda;
    private Boolean processado;
    private String mensagem;
}
