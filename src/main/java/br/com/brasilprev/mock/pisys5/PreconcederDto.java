package br.com.brasilprev.mock.pisys5;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PreconcederDto {

    private String matricula;
    private String beneficiario;
    private BigDecimal valorBeneficio;
    private String tipoRenda;

    public static Preconceder toEntity(PreconcederDto preconcederDto){
        return new Preconceder(preconcederDto.getMatricula(),
                                preconcederDto.getBeneficiario(),
                                preconcederDto.getValorBeneficio(),
                                preconcederDto.getTipoRenda(), Boolean.FALSE, null);
    }
}
