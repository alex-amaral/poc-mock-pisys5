package br.com.brasilprev.mock.pisys5;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@AllArgsConstructor
@RestController
public class PreconcedidoResource {

    private PreconcederRepository repository;

    @PostMapping("pre-conceder")
    public ResponseEntity<?> preConceder(@RequestBody List<PreconcederDto> preconcederList){

        System.out.println(" teste ... "+ preconcederList.size());
        preconcederList
                .forEach(p -> {
                	processar(PreconcederDto.toEntity(p));
                });
                
        

        return ok("Lista de preconcedido processado com sucesso.");
    }

    private Preconceder processar(Preconceder preconceder) {

    	System.out.println("Processado baixa de reserva");
    	System.out.println("Processado status");
    	System.out.println("Processado ativo");
    	System.out.println("Processado passivo");

        Preconceder preconcederSalvo = repository.save(preconceder);

        log.info("  ==>  <<Registrado para o sistema concedidos>>...");
        log.info(" ");

        return preconcederSalvo;
    }
}
